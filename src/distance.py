# Libraries
import asyncio
import time
import requests
import RPi.GPIO as GPIO

MAX_DISTANCE = 35
MIN_DISTANCE = 20
TOO_CLOSE = 10
status_before = False

# GPIO Mode (BOARD / BCM)
GPIO.setmode(GPIO.BCM)

# set GPIO Pins
GPIO_BLUE_LED = 21
GPIO_RED_LED = 22
GPIO_YELLOW_LED = 27
GPIO_GREEN_LED = 17
GPIO_TRIGGER = 18
GPIO_ECHO = 24

LED_LIST = [GPIO_BLUE_LED, GPIO_RED_LED, GPIO_YELLOW_LED, GPIO_GREEN_LED]

# set GPIO direction (IN / OUT)
GPIO.setup(GPIO_BLUE_LED, GPIO.OUT)
GPIO.setup(GPIO_RED_LED, GPIO.OUT)
GPIO.setup(GPIO_YELLOW_LED, GPIO.OUT)
GPIO.setup(GPIO_GREEN_LED, GPIO.OUT)
GPIO.setup(GPIO_TRIGGER, GPIO.OUT)
GPIO.setup(GPIO_ECHO, GPIO.IN)


def distance():
    # set Trigger to HIGH
    GPIO.output(GPIO_TRIGGER, True)

    # set Trigger after 0.01ms to LOW
    time.sleep(0.00001)
    GPIO.output(GPIO_TRIGGER, False)

    start_time = time.time()
    stop_time = time.time()

    # save StartTime
    while GPIO.input(GPIO_ECHO) == 0:
        start_time = time.time()

    # save time of arrival
    while GPIO.input(GPIO_ECHO) == 1:
        stop_time = time.time()

    # time difference between start and arrival
    time_elapsed = stop_time - start_time
    # multiply with the sonic speed (34300 cm/s)
    # and divide by 2, because there and back
    cm_distance = (time_elapsed * 34300) / 2
    return cm_distance


def status_led(cm_distance):
    if cm_distance < TOO_CLOSE:
        activate_led(GPIO_RED_LED)
    elif cm_distance < MIN_DISTANCE:
        activate_led(GPIO_YELLOW_LED)
    elif cm_distance < MAX_DISTANCE:
        activate_led(GPIO_GREEN_LED)
    else:
        activate_led(GPIO_BLUE_LED)


def send_update(status):
    global status_before
    print(status)
    print(status_before)
    if status_before != status:
        print("sending stuff")
        status_before = status
        requests.put("http://78.47.234.159/api/status/update", json={"id": "xxx", "activityStatus": str(status).lower()})


def activate_led(led):
    for i in LED_LIST:
        if led == i:
            GPIO.output(i, True)
        else:
            GPIO.output(i, False)


def context():
    try:
        while True:
            dist = distance()
            status_led(dist)
            print("Measured Distance = %.1f cm" % dist)
            time.sleep(0.1)
            send_update((dist < MAX_DISTANCE) and (dist > MIN_DISTANCE))

    # Reset by pressing CTRL + C
    except KeyboardInterrupt:
        print("Measurement stopped by User")
        GPIO.cleanup()


context()
